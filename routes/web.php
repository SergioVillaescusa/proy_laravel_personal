<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('socios', 'SociosController@index')->name('socios.index');

Route::get('socios/create', 'SociosController@create')->name('socios.create');

Route::post('socios/create', 'SociosController@store')->name('socios.store');

Route::get('socios/edit/{id}', 'SociosController@edit')->name('socios.edit');

Route::put('socios/update/{id}', 'SociosController@update')->name('socios.update');

Route::delete('socios/destroy{id}', 'SociosController@destroy')->name('socios.destroy');

Route::get('socios/show/{id}', 'SociosController@show')->name('socios.show');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');